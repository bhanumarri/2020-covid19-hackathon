# Second Debian Med COVID-19 hackathon

Planning the second Debian-Med COVID-19 hackathon

Dear Debian Community,

Debian Med joined the virtual (online) [COVID-19 Biohackathon from April 5-11](https://github.com/virtual-biohackathons/covid-19-bh20),
2020.  We considered the outcome a great success in terms of
the approached tasks, the new members we gained and the support
of Debian infrastructure teams (namely the ftpmaster team).

COVID-19 is not over and the Debian Med team wants to do another
week of hackathon to continue with this great success.  We want
to do this from June 15th to June 21th 2020.

A [recently shared pre-publication draft paper](https://doi.org/10.20944/preprints202005.0376.v1)
highlights which software tools are considered useful "to Accelerate SARS-CoV-2 and Coronavirus Research".
Many of these tools would benefit from being packaged in Debian and all the advantages that Debian brings for both users and upstream alike.

As in the first sprint most tasks do not require any knowledge of biology or medicine, and all
types of contributions are welcome: bug triage, testing, documentation,
CI, translations, packaging, and code contributions.

1. [Debian related bugs in COVID-19 related packages](https://blends.debian.org/med/bugs/covid-19.html)

2. [COVID-19 related software that is awaiting packaging](https://blends.debian.org/med/tasks/covid-19),
please respond to the RFP with your intent so we don't duplicate work

3. You can also contribute directly to the upstream packages, linked
from the [Debian Med COVID-19 task page](https://blends.debian.org/med/tasks/covid-19). Note:
many biomedical software packages are quite resource limited, even
compared to a typical FOSS project. Please be kind to the upstream
author/maintainers and realize that they may have limited resources to
review your contribution. Triaging open issues and opening pull requests
to fix problems is likely to be more useful than nitpicking their coding
style.

4. Architectures/porting: Please focus on amd64, as it is the primary
architecture for biomedical software. A secondary tier would be arm64 /
ppc64el / s390x (but beware the endian-related issues on s390x). From a
free/open hardware perspective it would be great to see more riscv64
support, but that is not a priority right now

5. Python developers: The Debian Med team is also trying to [improve the availability of
automated biomedical pipelines/workflows](https://doi.org/10.1007/s41019-017-0050-4) using the
Common Workflow Language open standard. The reference implementation of
CWL is written in Python and there are many [open issues ready for work
that don't require any biomedical background](https://github.com/common-workflow-language/cwltool/issues).

6. It is very easy to contribute to Debian Med team. We have a lowNMU
policy for all our packages. Merge requests on Salsa are usually
processed quickly (but please ping some of the latest Uploaders of the
package to make sure it will be noticed). Even better if you ask for
membership to the team and push directly to the salsa repository.

7. The [debian-med-team-policy](https://med-team.pages.debian.net/policy/) should answer all questions how to
contribute.

8. There is a [work-needed wiki](https://salsa.debian.org/med-team/community/2020-covid19-hackathon/-/wikis/COVID-19-Hackathon-packages-needing-work) that will help keep track of who is working on which projects.

9. There is also a [NEW requests wiki](https://salsa.debian.org/med-team/community/2020-covid19-hackathon/-/wikis/NEW-Requests) where we can request expedited NEW processing to support this effort.

During the hackathon we will coordinate ourselves via the the Salsa coordination page, Debian Med mailing list and IRC:

*  https://salsa.debian.org/med-team/community/2020-covid19-hackathon/-/wikis/Covid-19-hackathon
*  https://lists.debian.org/debian-med/
*  https://wiki.debian.org/IRC
*  irc://irc.debian.org/debian-med
*  https://jitsi.debian.social/DebianMedCovid19 every day at 15:00 UTC
